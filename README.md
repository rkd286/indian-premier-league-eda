# EDA on IPL Dataset

The **Indian Premier League** (IPL) is a professional Twenty20 cricket league in India contested during March or April and May of every year by eight teams representing eight different cities in India.[3] The league was founded by the **Board of Control for Cricket in India (BCCI)** in 2008.

# Content
- All Indian Premier League Cricket matches between 2008 and 2016.

- This is the ball by ball data of all the IPL cricket matches till season 9.

- The dataset contains 2 files: deliveries.csv and matches.csv.

- matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.

- deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

# Data Source
Kaggle
**URL -** https://www.kaggle.com/manasgarg/ipl
